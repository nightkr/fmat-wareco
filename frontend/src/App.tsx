import { A, Route, Routes } from "@solidjs/router";
import { Stockpiles } from "./pages/Stockpiles";
import { Production } from "./pages/Production";

function App() {
  return (
    <>
      <nav>
        <ul>
          <li>
            <A href="/stockpiles">stockpiles</A>
          </li>
          <li>
            <A href="/production">production</A>
          </li>
        </ul>
      </nav>
      <Routes>
        <Route path="/" component={() => "hello world"} />
        <Route path="/stockpiles" component={Stockpiles} />
        <Route path="/production" component={Production} />
      </Routes>
    </>
  );
}

export default App;
