import { For, Show, createResource } from "solid-js";

export const Production = () => {
  const [skus] = createResource(async () =>
    (await fetch("/api/sku/all")).json(),
  );
  const vehicleSkus = () =>
    (skus() || []).filter((sku: any) => sku.type === "vehicle");
  const skuMap = () =>
    Object.fromEntries((skus() || []).map((sku: any) => [sku.id, sku]));
  const [inventories, { refetch: refetchStockpiles }] = createResource(
    async () => (await fetch("/api/account/all/inventories")).json(),
  );
  const stockpiles = () =>
    (inventories() || []).filter((inv: any) => inv.type !== "ticket");
  const [ledger, { refetch: refetchLedger }] = createResource(async () =>
    (await fetch("/api/account/ledger/production")).json(),
  );
  const manu = async (sku: string, manufacturing_account: string) => {
    await fetch("/api/account/manufacture", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ sku, manufacturing_account }),
    });
    refetchStockpiles();
    refetchLedger();
  };
  const deliverToStockpile = async (
    sku: string,
    manufacturing_account: string,
  ) => {
    await fetch("/api/account/deliver/stockpile", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ sku, manufacturing_account }),
    });
    refetchStockpiles();
  };
  return (
    <div>
      <h2>Production</h2>
      <table>
        <thead>
          <tr>
            <th>Location</th>
            <For each={vehicleSkus()}>{(sku) => <th>{sku.name}</th>}</For>
          </tr>
        </thead>
        <tbody>
          <For each={stockpiles()}>
            {(stockpile) => (
              <tr>
                <th>{stockpile.name}</th>
                <For each={vehicleSkus()}>
                  {(sku) => (
                    <td>
                      {stockpile.inventory[sku.id]}
                      <Show
                        when={
                          (stockpile.type === "factory" &&
                            sku.vehicle_variant_of == undefined) ||
                          (stockpile.type === "upgrade" &&
                            sku.vehicle_variant_of)
                        }
                      >
                        <br />
                        <button onclick={() => manu(sku.id, stockpile.id)}>
                          manufacture
                        </button>
                        <br />
                        <button
                          onclick={() =>
                            deliverToStockpile(sku.id, stockpile.id)
                          }
                        >
                          deliver
                        </button>
                      </Show>
                    </td>
                  )}
                </For>
              </tr>
            )}
          </For>
        </tbody>
      </table>
      <h3>Ledger</h3>
      <table>
        <thead>
          <tr>
            <th>Time</th>
            <th>Location</th>
            <th>User</th>
            <th>Vehicle</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          <For each={ledger()}>
            {(entry) => (
              <tr>
                <td>{entry.performed_at}</td>
                <td>{entry.account_name}</td>
                {/* <td>{entry.added_by}</td> */}
                <td>Jaroslav</td>
                <td>{skuMap()[entry.sku]?.name ?? entry.sku}</td>
                <td>{entry.amount}</td>
              </tr>
            )}
          </For>
        </tbody>
      </table>
    </div>
  );
};
