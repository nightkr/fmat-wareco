import { For, createResource } from "solid-js";

export const Stockpiles = () => {
  const [skus] = createResource(async () =>
    (await fetch("/api/sku/all")).json(),
  );
  const [stockpiles] = createResource(async () =>
    (await fetch("/api/account/all/inventories")).json(),
  );
  return (
    <div>
      <h2>Stockpiles</h2>
      <table>
        <thead>
          <tr>
            <th>Stockpile</th>
            <For each={skus()}>{(sku) => <th>{sku.name}</th>}</For>
          </tr>
        </thead>
        <tbody>
          <For each={stockpiles()}>
            {(stockpile) => (
              <tr>
                <th>{stockpile.name}</th>
                <For each={skus()}>
                  {(sku) => <td>{stockpile.inventory[sku.id]}</td>}
                </For>
              </tr>
            )}
          </For>
        </tbody>
      </table>
    </div>
  );
};
