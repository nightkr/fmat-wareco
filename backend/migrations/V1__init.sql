create table users(
  id uuid not null primary key,
  discord_user_id text
);

create type account_type as enum('stockpile', 'factory', 'upgrade', 'ticket');
create table accounts(
  id uuid not null primary key,
  weight int,
  name text not null,
  type account_type not null,
  manufacturing_for uuid references accounts(id)
);

create type sku_type as enum('material', 'vehicle');

create table skus(
  id uuid not null primary key,
  name text not null,
  type sku_type not null,
  vehicle_variant_of uuid references skus(id)
);

create table orders(
  id uuid not null primary key,
  added_at timestamp not null default now(),
  added_by uuid not null references users(id),
  ticket_number int unique,
  account uuid references accounts(id)
);

create table order_entries(
  id uuid not null primary key,
  "order" uuid not null references orders(id),
  sku uuid not null references skus(id),
  amount int not null,
  price_per_unit int not null,
  discount_on_total int not null
);

create table ledger_entries(
  id uuid not null primary key,
  version serial not null,
  performed_at timestamp not null default now(),
  added_by uuid not null references users(id),
  debit_account uuid references accounts(id),
  debit_sku uuid references skus(id),
  debit_amount int not null,
  credit_account uuid references accounts(id),
  credit_sku uuid references skus(id),
  credit_amount int not null
);
