insert into users(id) values
  ('2e4ac72e-c2e6-46dc-bdc8-622c460c314c');

insert into accounts(id, weight, name, type, manufacturing_for)
values
  ('130196ac-9cac-4942-b36f-768325b3008f', 0, 'Seaport (Scrying Belt)', 'stockpile', null),
  ('41e4f9b1-fabc-47b9-bef1-306a21ccdf7e', 1, 'MPF (Scrying Belt)', 'factory', '130196ac-9cac-4942-b36f-768325b3008f'),
  ('c69a873b-d6a1-414f-bae2-860a5a3f5492', 2, 'Upgrade Pads (Scrying Belt)', 'upgrade', '130196ac-9cac-4942-b36f-768325b3008f'),
  ('277e6fd1-f9b8-4e89-9ff3-6fe165831e14', 3, 'Seaport (Ulster Falls)', 'stockpile', null),
  ('60cb33d7-c8c8-4f5b-9de4-cb99143956b2', 4, 'MPF (Ulster Falls)', 'factory', '277e6fd1-f9b8-4e89-9ff3-6fe165831e14'),
  ('6bae0cf4-10cf-423f-acf8-6d6e9bff8ee2', 5, 'Upgrade Pads (Ulster Falls)', 'upgrade', '277e6fd1-f9b8-4e89-9ff3-6fe165831e14'),
  ('d65d9190-31b2-43ee-adde-40bdc3538ba8', null, 'Ticket 1234', 'ticket', null),
  ('54acd34a-14fc-4ade-a6c3-5ae44ef5e5f7', null, 'Ticket 1235', 'ticket', null);

insert into orders(id, account, added_by)
values
  ('5d601899-435a-4f34-b81f-189c4c01b0b3', 'd65d9190-31b2-43ee-adde-40bdc3538ba8', '2e4ac72e-c2e6-46dc-bdc8-622c460c314c'),
  ('2173d611-e50e-4bb5-a060-fd51a12011d8', '54acd34a-14fc-4ade-a6c3-5ae44ef5e5f7', '2e4ac72e-c2e6-46dc-bdc8-622c460c314c');

insert into skus(id, name, type, vehicle_variant_of) values
                             ('47e1add7-db58-4c3c-aceb-46bd7eebdd7f', 'rmat', 'material', null),
                             ('4cbce2a6-459d-4c5b-b7da-9a8787337f97', 'widow', 'vehicle', null),
                             ('7cb34ff5-81a5-43ad-bf7f-82101b4fd7ff', 'firebrand', 'vehicle', '4cbce2a6-459d-4c5b-b7da-9a8787337f97');

insert into ledger_entries(id, added_by, debit_account, debit_sku, debit_amount, credit_account, credit_sku, credit_amount)
values
  ('e30de319-f468-4153-b11e-e3a55ebe8993', '2e4ac72e-c2e6-46dc-bdc8-622c460c314c',
  '41e4f9b1-fabc-47b9-bef1-306a21ccdf7e', '4cbce2a6-459d-4c5b-b7da-9a8787337f97', 2,
  '130196ac-9cac-4942-b36f-768325b3008f', '4cbce2a6-459d-4c5b-b7da-9a8787337f97', 2),
  ('fec9c303-a8c3-4a85-88b9-e9d0d36cb716', '2e4ac72e-c2e6-46dc-bdc8-622c460c314c',
  '130196ac-9cac-4942-b36f-768325b3008f', '4cbce2a6-459d-4c5b-b7da-9a8787337f97', 1,
  'c69a873b-d6a1-414f-bae2-860a5a3f5492', '7cb34ff5-81a5-43ad-bf7f-82101b4fd7ff', 1),
  ('a8e128f4-8d42-4369-bd0b-8d7e2b824177', '2e4ac72e-c2e6-46dc-bdc8-622c460c314c',
  'd65d9190-31b2-43ee-adde-40bdc3538ba8', '4cbce2a6-459d-4c5b-b7da-9a8787337f97', 1,
  'd65d9190-31b2-43ee-adde-40bdc3538ba8', '47e1add7-db58-4c3c-aceb-46bd7eebdd7f', 100);
