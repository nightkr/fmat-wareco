use std::{collections::HashMap, sync::Arc};

use axum::{extract::State, routing::get, Json, Router};
use serde::Serialize;
use tokio_postgres::types::Json as PgJson;
use uuid::Uuid;

use crate::AppState;

pub fn router() -> Router<Arc<AppState>> {
    Router::new().route("/all", get(all))
}

#[derive(Serialize)]
struct Sku {
    id: Uuid,
    name: String,
    #[serde(rename = "type")]
    ty: String,
    vehicle_variant_of: Option<Uuid>,
}

async fn all(State(state): State<Arc<AppState>>) -> Json<Vec<Sku>> {
    let db = state.db.get().await.unwrap();
    Json(
        db.query(include_str!("all.sql"), &[])
            .await
            .unwrap()
            .into_iter()
            .map(|row| Sku {
                id: row.get("id"),
                name: row.get("name"),
                ty: row.get("type"),
                vehicle_variant_of: row.get("vehicle_variant_of"),
            })
            .collect(),
    )
}
