mod accounts;
mod skus;
#[cfg(feature = "ui")]
mod ui;

use std::{net::SocketAddr, sync::Arc};

use axum::Router;
use clap::Parser;
use deadpool_postgres::Hook;
use refinery::embed_migrations;
use serde::{Deserialize, Serialize};
use tokio_postgres::NoTls;

embed_migrations!("./migrations");

#[derive(clap::Parser)]
struct Args {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(clap::Parser)]
enum Cmd {
    Migrate,
    Run {
        #[clap(long, default_value = "0.0.0.0:3000")]
        addr: SocketAddr,
    },
}

#[derive(Serialize, Deserialize)]
struct Config {
    pg: deadpool_postgres::Config,
}

pub struct AppState {
    db: deadpool_postgres::Pool,
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let config = config::Config::builder()
        .add_source(config::File::with_name("config"))
        .add_source(config::Environment::default().separator("__"))
        .build()
        .unwrap()
        .try_deserialize::<Config>()
        .unwrap();
    let args = Args::parse();
    match args.cmd {
        Cmd::Migrate => {
            let db = config.pg.create_pool(None, NoTls).unwrap();
            let mut db = db.get().await.unwrap();
            migrations::runner().run_async(&mut **db).await.unwrap();
        }
        Cmd::Run { addr } => {
            let db = config
                .pg
                .builder(NoTls)
                .unwrap()
                .post_create(Hook::async_fn(|db, _| {
                    Box::pin(async {
                        db.batch_execute(include_str!("views.sql")).await.unwrap();
                        Ok(())
                    })
                }))
                .build()
                .unwrap();
            let api = Router::new()
                .nest("/account", accounts::router())
                .nest("/sku", skus::router());
            let app = Router::<Arc<AppState>>::new().nest("/api", api);
            #[cfg(feature = "ui")]
            let app = app.merge(ui::router());
            let app = app.with_state(Arc::new(AppState { db }));
            tracing::info!(%addr, "starting web server");
            axum::Server::bind(&addr)
                .serve(app.into_make_service())
                .await
                .unwrap();
        }
    }
}
