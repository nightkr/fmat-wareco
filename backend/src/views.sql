create or replace temporary view account_sku_inventory as
  select account.id as account, sku.id as sku, coalesce(credit.sum, 0) - coalesce(debit.sum, 0) as amount
    from accounts as account
         cross join skus as sku
         left outer join (
           select entry.debit_account as account, entry.debit_sku as sku,
                  sum(entry.debit_amount) as sum
             from ledger_entries as entry
            group by account, sku) as debit on debit.account = account.id and debit.sku = sku.id
         left outer join (
           select entry.credit_account as account, entry.credit_sku as sku,
                  sum(entry.credit_amount) as sum
             from ledger_entries as entry
            group by account, sku) as credit on credit.account = account.id and credit.sku = sku.id;

select * from account_sku_inventory;
