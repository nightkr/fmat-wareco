insert into ledger_entries(id, added_by, debit_account, debit_sku, debit_amount, credit_account, credit_sku, credit_amount)
values($1, '2e4ac72e-c2e6-46dc-bdc8-622c460c314c', (select manufacturing_for from accounts where id = $3), $2, 1, $3, $4, 1);
