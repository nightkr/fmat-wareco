select *,
       (select sum(inventory.amount)
          from account_sku_inventory as inventory
         where inventory.account = "order".account
           and inventory.amount > 0) as missing_payments,
       (select -sum(inventory.amount)
          from account_sku_inventory as inventory
         where inventory.account = "order".account
           and inventory.amount < 0) as missing_deliveries
  from orders as "order"
