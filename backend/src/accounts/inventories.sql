select account.id, account.name, account.type::text, account.manufacturing_for,
       jsonb_object_agg(inventory.sku, inventory.amount) as inventory
  from accounts as account
       inner join account_sku_inventory as inventory on account.id = inventory.account
 group by account.id
 order by account.weight asc
