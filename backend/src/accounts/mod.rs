use std::{collections::HashMap, sync::Arc};

use axum::{
    extract::State,
    routing::{get, post},
    Json, Router,
};
use serde::{Deserialize, Serialize};
use time::{OffsetDateTime, PrimitiveDateTime};
use tokio_postgres::types::Json as PgJson;
use uuid::Uuid;

use crate::AppState;

pub fn router() -> Router<Arc<AppState>> {
    Router::new()
        .route("/all/inventories", get(inventories))
        .route("/manufacture", post(manufacture))
        .route("/deliver/stockpile", post(deliver_to_stockpile))
        .route("/ledger/production", get(production_ledger))
}

#[derive(Serialize)]
struct AccountInventory {
    id: Uuid,
    name: String,
    #[serde(rename = "type")]
    ty: String,
    manufacturing_for: Option<Uuid>,
    inventory: HashMap<Uuid, i64>,
}

async fn inventories(State(state): State<Arc<AppState>>) -> Json<Vec<AccountInventory>> {
    let db = state.db.get().await.unwrap();
    Json(
        db.query(include_str!("inventories.sql"), &[])
            .await
            .unwrap()
            .into_iter()
            .map(|row| AccountInventory {
                id: row.get("id"),
                name: row.get("name"),
                ty: row.get("type"),
                manufacturing_for: row.get("manufacturing_for"),
                inventory: row.get::<_, PgJson<_>>("inventory").0,
            })
            .collect(),
    )
}

#[derive(Deserialize)]
struct ManufactureRequest {
    sku: Uuid,
    manufacturing_account: Uuid,
}

async fn manufacture(
    State(state): State<Arc<AppState>>,
    Json(req): Json<ManufactureRequest>,
) -> Json<()> {
    let db = state.db.get().await.unwrap();
    let sku = db
        .query_one(
            "select vehicle_variant_of from skus where id=$1",
            &[&req.sku],
        )
        .await
        .unwrap();
    if let Some(vehicle_variant_of) = sku.get::<_, Option<Uuid>>("vehicle_variant_of") {
        db.execute(
            include_str!("upgrade.sql"),
            &[
                &Uuid::new_v4(),
                &vehicle_variant_of,
                &req.manufacturing_account,
                &req.sku,
            ],
        )
        .await
        .unwrap();
    } else {
        db.execute(
            include_str!("manufacture.sql"),
            &[&Uuid::new_v4(), &req.manufacturing_account, &req.sku],
        )
        .await
        .unwrap();
    }
    Json(())
}

#[derive(Deserialize)]
struct DeliverToStockpileRequest {
    sku: Uuid,
    manufacturing_account: Uuid,
}

async fn deliver_to_stockpile(
    State(state): State<Arc<AppState>>,
    Json(req): Json<DeliverToStockpileRequest>,
) -> Json<()> {
    let db = state.db.get().await.unwrap();
    db.execute(
        include_str!("deliver-to-stockpile.sql"),
        &[&Uuid::new_v4(), &req.manufacturing_account, &req.sku],
    )
    .await
    .unwrap();
    Json(())
}

#[derive(Serialize)]
struct ProductionLedgerEntry {
    performed_at: PrimitiveDateTime,
    account_name: String,
    added_by: Uuid,
    sku: Uuid,
    amount: i64,
}

async fn production_ledger(State(state): State<Arc<AppState>>) -> Json<Vec<ProductionLedgerEntry>> {
    let db = state.db.get().await.unwrap();
    Json(
        db.query(include_str!("production-ledger.sql"), &[])
            .await
            .unwrap()
            .into_iter()
            .map(|row| ProductionLedgerEntry {
                performed_at: row.get("performed_at_time"),
                account_name: row.get("account_name"),
                added_by: row.get("added_by"),
                sku: row.get("sku"),
                amount: row.get("amount"),
            })
            .collect(),
    )
}
