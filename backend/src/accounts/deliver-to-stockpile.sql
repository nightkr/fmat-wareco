insert into ledger_entries(id, added_by, debit_account, debit_sku, credit_account, credit_sku, debit_amount, credit_amount)
values($1, '2e4ac72e-c2e6-46dc-bdc8-622c460c314c',
       $2,
       $3,
       (select manufacturing_for from accounts where id=$2),
       $3,
       (select case when vehicle_variant_of is null then 3 else 1 end from skus where id=$3),
       (select case when vehicle_variant_of is null then 3 else 1 end from skus where id=$3));
