select date_trunc('minute', entry.performed_at) as performed_at_time,
       account.name as account_name,
       entry.added_by,
       entry.credit_sku as sku,
       sum(entry.credit_amount) as amount
  from accounts as account
       inner join ledger_entries as entry on entry.credit_account = account.id
 where account.manufacturing_for is not null
 group by performed_at_time, account_name, added_by, sku
 order by performed_at_time desc
